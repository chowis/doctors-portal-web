import { Box, Button, Dialog, DialogContent, DialogTitle, Grid } from '@material-ui/core'
import { useRequest } from 'ahooks'
import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { useTranslation } from 'react-i18next'

import { useAPI } from '../api/API'

interface ConfirmationProps {
  isAsync?: boolean
  url?: string
  title: string
  onClose?: () => void
  onOpen?: () => void
  onYes?: () => void
  onNo?: () => void
}

export function useConfirmation(props: ConfirmationProps) {
  const portalContainer = document.getElementById('portals')
  const { t } = useTranslation()
  const api = useAPI()
  const [body, setBody] = useState<unknown>()
  const [showConfirmation, setShowConfirmation] = useState(false)

  const deleteRequest = useRequest(
    (data: unknown) => api.requestJSON('DELETE', props.url || '', { data }),
    {
      manual: true,
      onSuccess: () => {
        closeModal()
      },
    }
  )

  const closeModal = () => {
    if (!deleteRequest.loading) {
      setShowConfirmation(false)
      props.onClose?.()
    }
  }

  const openModal = (b: unknown) => {
    setBody(b)
    setShowConfirmation(true)
    props.onOpen?.()
  }

  if (portalContainer) {
    ReactDOM.render(
      <Dialog open={showConfirmation} onClose={closeModal}>
        <DialogTitle>{props.title}</DialogTitle>
        <DialogContent>
          <Box marginBottom={2}>
            <Grid container spacing={2}>
              <Grid item xs>
                <Button
                  fullWidth
                  variant="outlined"
                  disabled={deleteRequest.loading}
                  onClick={() => {
                    closeModal()
                    props.onNo?.()
                  }}
                >
                  {t('login.no')}
                </Button>
              </Grid>
              <Grid item xs>
                <Button
                  fullWidth
                  color="secondary"
                  variant="outlined"
                  onClick={() => {
                    props.onYes?.()

                    if (props.isAsync) {
                      deleteRequest.run(body)
                    }
                  }}
                  disabled={deleteRequest.loading}
                >
                  {t('login.yes')}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogContent>
      </Dialog>,
      portalContainer
    )
  }

  return { openModal, closeModal }
}
