/* eslint-disable @typescript-eslint/naming-convention */
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
} from '@material-ui/core'
import { Bookmark, ListAlt, NoteAdd, People, PowerSettingsNew } from '@material-ui/icons'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useRouteMatch } from 'react-router-dom'

import { version } from '../../package.json'
import { useAccessFlags } from '../data/AccessFlags'
import { APP_LANGUAGES, useAppLanguage } from '../i18n/hooks'

export function Sidebar() {
  const match = useRouteMatch()
  const { t } = useTranslation()
  const [currentLanguage, setLanguage] = useAppLanguage()
  const [showLogoutConfirmation, setShowLogoutConfirmation] = useState(false)
  const history = useHistory()
  const { can_add_products, has_access_to_patients, has_access_to_orders } = useAccessFlags()

  return (
    <Box borderRight="1px solid #dcdcdc" height="100%" display="flex" flexDirection="column">
      <Dialog
        open={showLogoutConfirmation}
        onClose={() => {
          setShowLogoutConfirmation(false)
        }}
      >
        <DialogTitle>{t('login.logout_confirmation_msg')}</DialogTitle>
        <DialogContent>
          <Box marginBottom={2}>
            <Grid container spacing={2}>
              <Grid item xs>
                <Button
                  fullWidth
                  variant="outlined"
                  onClick={() => {
                    setShowLogoutConfirmation(false)
                  }}
                >
                  {t('login.no')}
                </Button>
              </Grid>
              <Grid item xs>
                <Button
                  fullWidth
                  color="secondary"
                  variant="outlined"
                  onClick={() => {
                    history.push('/logout')
                  }}
                >
                  {t('login.yes')}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogContent>
      </Dialog>

      <Box width="200px" padding="16px">
        <Typography variant="h5">DERMCONCEPT</Typography>
        <Typography variant="subtitle1">Doctor's Portal</Typography>
      </Box>
      <Divider />
      <List>
        {has_access_to_patients && (
          <ListItem
            button
            component={Link}
            to="/patients"
            selected={match.path.includes('/patients')}
          >
            <ListItemIcon>
              <People />
            </ListItemIcon>
            <ListItemText primary="Patient List" />
          </ListItem>
        )}

        {has_access_to_orders && (
          <ListItem button component={Link} to="/orders" selected={match.path.includes('/orders')}>
            <ListItemIcon>
              <Bookmark />
            </ListItemIcon>
            <ListItemText primary="Orders" />
          </ListItem>
        )}

        {can_add_products && (
          <ListItem
            button
            component={Link}
            to="/add-products"
            selected={match.path.includes('/add-products')}
          >
            <ListItemIcon>
              <NoteAdd />
            </ListItemIcon>
            <ListItemText primary="Add Products" />
          </ListItem>
        )}

        <ListItem
          button
          component={Link}
          to="/prescription-details"
          selected={match.path.includes('/prescription-details')}
        >
          <ListItemIcon>
            <ListAlt />
          </ListItemIcon>
          <ListItemText primary="Prescriptions" />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem
          button
          onClick={() => {
            setShowLogoutConfirmation(true)
          }}
        >
          <ListItemIcon>
            <PowerSettingsNew />
          </ListItemIcon>
          <ListItemText primary={t('sidebar.logout')} />
        </ListItem>
      </List>

      <Box paddingX={2} paddingY={4} display="flex" alignItems="flex-end" flexGrow={1}>
        <Grid container direction="column" alignItems="stretch" spacing={2}>
          <Grid item xs>
            <TextField
              label={currentLanguage !== 'en' ? `${t('language')}/Language` : t('language')}
              value={currentLanguage}
              select
              variant="outlined"
              size="small"
              fullWidth
              onChange={(event) => {
                setLanguage(event.target.value)
              }}
            >
              {APP_LANGUAGES.map(({ code, value }) => (
                <MenuItem key={code} value={code}>
                  {value}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item>
            <Typography color="textSecondary" align="center">
              {t('version')}: {version}
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}
