import { useUserState } from './UserState'

type Roles =
  | 'Doctor'
  | 'Logistics'
  | 'Pharmacist'
  | undefined

export function useAccessFlags() {
  const { consultant_position: { name } = {} } = useUserState()
  const position = name as Roles

  return {
    has_access_to_prescriptions: position === 'Doctor' || position === 'Logistics',
    has_access_to_patients:
      position === 'Doctor',
    can_send_to_patient: position === 'Logistics' || position === 'Doctor',
    can_add_products: position === 'Doctor',
    has_access_to_orders: position === 'Logistics' || position === 'Doctor',
  }
}
