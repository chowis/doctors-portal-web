import { format, parse } from 'date-fns'

export function parseDateString(date: string, fallback?: string) {
  try {
    let val = date
    if (!val) {
      return fallback
    }

    if (val.length > 8) {
      val = val.substring(0, 8)
    }

    return format(parse(val, 'yyyyMMdd', new Date()), 'yyyy-MM-dd')
  } catch (error) {
    return fallback
  }
}


export function calculateAge(birthday: Date) {
  const ageDifMs = Date.now() - birthday.getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}


export function formatDate(date: Date) {
  const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
  const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
  const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);

  return `${ye}${mo}${da}`;
}