/* eslint-disable @typescript-eslint/naming-convention */
import './GlobalStyles.css'
import './i18n/config'
import 'core-js/es/promise'
import 'core-js/es/set'
import 'core-js/es/map'

import { LocalizationProvider } from '@material-ui/pickers'
import DateFnsAdapter from '@material-ui/pickers/adapter/date-fns'
import enLocale from 'date-fns/locale/en-US'
import { SnackbarProvider } from 'notistack'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { QueryParamProvider } from 'use-query-params'

import { Login } from './auth/Login'
import { Logout } from './auth/Logout'
import { ProtectedRoutes } from './auth/ProtectedRoute'
import { useAccessFlags } from './data/AccessFlags'
import { AppContextProvider } from './data/AppContext'
import AddPrescription from './pages/AddPrescription'
import AddProducts from './pages/AddProducts'
import AnalysisDetailsPage from './pages/AnalysisDetails'
import AnalysisHistoryPage from './pages/AnalysisHistoryPage'
import Orders from './pages/Orders'
import PatientsList from './pages/PatientsList'
import { PrescriptionDetails } from './pages/PrescriptionDetails'
import reportWebVitals from './reportWebVitals'

const localeMap = {
  en: enLocale,
}

function App() {
  const { has_access_to_patients } = useAccessFlags()
  return (
    <React.StrictMode>
      <LocalizationProvider dateAdapter={DateFnsAdapter} locale={localeMap.en}>
        <SnackbarProvider maxSnack={1}>
          <AppContextProvider>
            <BrowserRouter>
              <QueryParamProvider ReactRouterRoute={Route}>
                <Route path="/login">
                  <Login />
                </Route>

                <ProtectedRoutes>
                  <Switch>
                    <Route path="/patients/:customer_id/:analysis_id/:batch_id">
                      <AnalysisDetailsPage />
                    </Route>

                    <Route path="/patients/analysis/:patientId">
                      <AnalysisHistoryPage />
                    </Route>

                    <Route path="/patients">
                      <PatientsList />
                    </Route>

                    <Route path="/orders">
                      <Orders />
                    </Route>

                    <Route path="/add-products">
                      <AddProducts />
                    </Route>

                    <Route path="/add-prescription/:patientId/:batchId">
                      <AddPrescription />
                    </Route>

                    <Route path="/prescription-details">
                      <PrescriptionDetails />
                    </Route>

                    <Route path="/logout">
                      <Logout />
                    </Route>

                    <Redirect
                      exact
                      from="/"
                      to={has_access_to_patients ? '/patients' : '/prescription-details'}
                    />
                  </Switch>
                </ProtectedRoutes>
              </QueryParamProvider>
            </BrowserRouter>
          </AppContextProvider>
        </SnackbarProvider>
      </LocalizationProvider>
    </React.StrictMode>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
