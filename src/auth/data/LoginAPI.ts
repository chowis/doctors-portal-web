import { useAPI } from '../../api/API'
import { LoginDTO, UserStateDTO, userStateSchema } from './LoginDTO'

export function useLoginAPI() {
  const { requestJSON } = useAPI()

  return {
    login: (values: LoginDTO) =>
      requestJSON<LoginDTO, UserStateDTO>('POST', 'auth/login/', {
        data: values,
      }).then((data) => userStateSchema.cast(data.data)),
  }
}
