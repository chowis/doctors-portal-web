import { Box, Button, Dialog, DialogContent, DialogTitle, Grid, TextField } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { useRequest } from 'ahooks'
import { useSnackbar } from 'notistack'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { useAPI } from '../api/API'

interface ForgotPasswordProps {
  open: boolean
  onClose: () => void
}

export function ForgotPassword({ open, onClose }: ForgotPasswordProps) {
  const { t } = useTranslation()
  const [error, setError] = useState()
  const form = useForm<{ email: string }>({
    defaultValues: {
      email: '',
    },
  })
  const { enqueueSnackbar } = useSnackbar()
  const api = useAPI()

  const onSubmit = useRequest(
    (values: { email: string }) =>
      api.requestJSON('POST', '/auth/forgot-password/', {
        data: { email: values.email },
      }),
    {
      manual: true,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onSuccess: (e: any) => {
        if (e) {
          enqueueSnackbar(t('forgot_password.recover_email_sent_msg'), {
            variant: 'success',
          })
          onClose()
        }
      },
      onError: (e) => {
        if (e) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const message = e.message as any
          setError(message.en)
        }
      },
    }
  )

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t('forgot_password.title')}</DialogTitle>
      <DialogContent>
        <form onSubmit={form.handleSubmit(onSubmit.run)}>
          {!!error && (
            <Box paddingBottom={2}>
              <Alert severity="error">{error}</Alert>
            </Box>
          )}
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <TextField
                label={t('forgot_password.email')}
                variant="outlined"
                size="small"
                fullWidth
                helperText={form.errors.email?.message || t('forgot_password.helper_text')}
                name="email"
                type="email"
                inputRef={form.register({ required: t('forgot_password.email_required_msg') })}
                error={!!form.errors.email?.message}
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                disabled={form.formState.isSubmitting || onSubmit.loading}
                disableElevation
                variant="contained"
                color="primary"
                fullWidth
              >
                {t('forgot_password.recover')}
              </Button>
            </Grid>
          </Grid>
        </form>
        <Box marginTop={2} />
      </DialogContent>
    </Dialog>
  )
}
