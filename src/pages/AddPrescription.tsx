import { yupResolver } from '@hookform/resolvers/yup'
import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@material-ui/core'
import { Delete, Send } from '@material-ui/icons'
import { useRequest } from 'ahooks'
import { useSnackbar } from 'notistack'
import React, { useMemo, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom'

import { useAPI } from '../api/API'
import { Layout } from '../components/Layout'
import { useUserState } from '../data/UserState'
import { calculateAge, formatDate, parseDateString } from '../helpers/dateHelpers'
import { objectSchema, stringSchema } from '../helpers/SchemaHelpers'
import { parseGender } from '../helpers/utils'
import { Product } from './AddProducts'
import { AnalysisHistoryDetails } from './AnalysisHistoryPage'

type Pharmacist = {
  Branch_id: number
  Company_id: number
  Rank_id: number
  Store_id: number
  address: null | string
  birthdate: null | string
  city: null | string
  country: null | string
  email: string
  gender: string
  id: number
  is_active: number
  loginserver_id: number
  memo: string
  name: string
  phone: string
  programs_id: number
  register_date: string
  surname: string
}

interface AddProductsDialogProps {
  open: boolean
  productsInPrescription: Map<string, Product>
  onClose: () => void
  onSubmit: (p: Map<string, Product>) => void
}

function AddProductsDialog({
  open,
  productsInPrescription,
  onClose,
  onSubmit,
}: AddProductsDialogProps) {
  const api = useAPI()
  const products = useRequest(() => api.requestResource<Product[]>('/products'))
  const [selectedProducts, setSelectedProducts] = useState<Map<string, Product>>(
    productsInPrescription
  )

  return (
    <Dialog open={open} onClose={onClose} maxWidth="lg">
      <DialogTitle>Add Products to Prescription</DialogTitle>
      <DialogContent>
        <TableContainer component={Paper}>
          <Table aria-label="simple table" stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell>Product Code</TableCell>
                <TableCell>Product Name</TableCell>
                <TableCell>Description</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {products.data?.data.map((row) => (
                <TableRow
                  key={row.id}
                  hover
                  onClick={() => {
                    const nextSelectedProducts = new Map(selectedProducts)
                    if (nextSelectedProducts.has(row.id)) {
                      nextSelectedProducts.delete(row.id)
                    } else {
                      nextSelectedProducts.set(row.id, row)
                    }
                    setSelectedProducts(nextSelectedProducts)
                  }}
                >
                  <TableCell>{row.code}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>
                    <Checkbox checked={selectedProducts.has(row.id)} />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DialogContent>
      <DialogActions>
        <Box margin={2} display="flex" justifyContent="flex-end">
          <Button variant="outlined" onClick={onClose}>
            Cancel
          </Button>{' '}
          <Box marginLeft={2} />
          <Button
            variant="outlined"
            color="primary"
            onClick={() => {
              onSubmit(selectedProducts)
              onClose()
            }}
          >
            Add
          </Button>
        </Box>
      </DialogActions>
    </Dialog>
  )
}

type Prescription = {
  patient_id: string
  batch_id: string
  products: Array<{ id: string; volume: number }>
  notes: string
  date_time: string
  status: string
  pharmacist_email: string
}

interface SendPrescriptionDialogProps {
  open: boolean
  loading: boolean
  onClose: () => void
  onSubmit: (email: string) => void
}

function SendPrescriptionDialog({ open, loading, onClose, onSubmit }: SendPrescriptionDialogProps) {
  const api = useAPI()
  const pharmacists = useRequest<{ data: Pharmacist[] }>(() =>
    api.requestResource('/customer-record/pharmacists')
  )
  const form = useForm({
    defaultValues: { email: '' },
    resolver: yupResolver(objectSchema({ email: stringSchema().email().required() })),
  })
  return (
    <Dialog open={open} onClose={onClose} maxWidth="lg">
      <form
        onSubmit={form.handleSubmit((values) => {
          onSubmit(values.email)
        })}
      >
        <DialogContent>
          <Box width="300px">
            <Controller
              name="email"
              control={form.control}
              render={({ onChange, value, ref }) => (
                <TextField
                  select
                  inputRef={ref}
                  fullWidth
                  label="Select Pharmacist"
                  defaultValue=""
                  value={value}
                  onChange={(event) => onChange(event.target.value)}
                >
                  {pharmacists.data?.data.map((p) => (
                    <MenuItem key={p.id} value={p.email}>
                      {p.email}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Box margin={2}>
            <Grid container spacing={2} alignItems="center">
              {loading && (
                <Grid item>
                  <CircularProgress size={24} />
                </Grid>
              )}
              <Grid item>
                <Button disabled={loading} variant="outlined" fullWidth type="submit">
                  Send
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default function AddPrescription() {
  const [modalState, setModalState] = useState<'add_products' | 'send_prescription'>()
  const [productsInPrescription, setProductsInPrescription] = useState<Map<string, Product>>(
    new Map([])
  )
  const [notes, setNotes] = useState('')
  const api = useAPI()
  const params = useParams<{ patientId: string; batchId: string }>()
  const patient = useRequest<AnalysisHistoryDetails>(() =>
    api.requestResource(`/customer-record/${params.patientId}/analysis-history/customer/`)
  )
  const { enqueueSnackbar } = useSnackbar()
  const user = useUserState()
  const sendPrescriptionRequest = useRequest(
    (data: Prescription) => api.requestJSON('POST', '/prescriptions/add-prescription', { data }),
    {
      manual: true,
      onSuccess: () => {
        setProductsInPrescription(new Map([]))
        setModalState(undefined)
        setNotes('')
        enqueueSnackbar('Successfully sent', {
          variant: 'success',
        })
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onError: (error: any) => {
        if (error.error) {
          enqueueSnackbar(error.error, {
            variant: 'error',
          })
        } else {
          enqueueSnackbar(error.error, {
            variant: 'error',
          })
        }
      },
    }
  )
  const products = useMemo(() => Array.from(productsInPrescription.values()), [
    productsInPrescription,
  ])

  const birth = patient.data?.birth ? parseDateString(patient.data?.birth) : undefined

  return (
    <Layout title="Add Prescription">
      <AddProductsDialog
        open={modalState === 'add_products'}
        key={productsInPrescription.size}
        productsInPrescription={productsInPrescription}
        onClose={() => {
          setModalState(undefined)
        }}
        onSubmit={setProductsInPrescription}
      />

      <SendPrescriptionDialog
        open={modalState === 'send_prescription'}
        onClose={() => {
          setModalState(undefined)
        }}
        loading={sendPrescriptionRequest.loading}
        onSubmit={(pharmacist_email) => {
          sendPrescriptionRequest.run({
            patient_id: params.patientId,
            batch_id: params.batchId,
            date_time: formatDate(new Date()),
            products: products.map((p) => ({ id: p.id, volume: p.volume || 1 })),
            notes,
            pharmacist_email,
            status: 'in progress',
          })
        }}
      />

      <Box px="20px">
        <Grid container justify="space-between">
          <Grid container direction="column" justify="space-between">
            <Grid>
              <Typography variant="h6" align="center">
                Prescription for patient
              </Typography>

              <Box bgcolor="#eaeaea" p="20px 20px 10px 20px">
                <Grid container spacing={2} style={{ borderTop: '1px dashed #000' }}>
                  <Grid item>
                    <Typography>
                      <b>Patient ID: </b>
                      {patient.data?.id}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      <b>Name: </b>
                      {patient.data?.surname} {patient.data?.name}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      <b>Age: </b>
                      {birth ? calculateAge(new Date(birth)) : ''}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      <b>Gender: </b>
                      {parseGender(patient.data?.gender)}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      <b>Date: </b>
                      {birth}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>
                      <b>Details: </b>
                      {patient.data?.ethnicity}
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
            </Grid>

            <Grid>
              <Box my={3} px="50px">
                <Grid container justify="space-between" alignItems="center">
                  <Grid item>
                    <Typography variant="h5">Product Recommendation</Typography>
                  </Grid>
                  <Grid item>
                    <Button
                      onClick={() => {
                        setModalState('add_products')
                      }}
                      variant="contained"
                    >
                      Add Product
                    </Button>
                  </Grid>
                </Grid>
                <Box marginTop={2}>
                  <Grid container spacing={2}>
                    {products.map(({ id, name, code, volume, description }) => (
                      <Grid key={id} item container spacing={2} alignItems="center">
                        <Grid item xs={3}>
                          <TextField
                            InputProps={{ readOnly: true }}
                            size="small"
                            label="Product Code"
                            fullWidth
                            value={code || 'N/A'}
                          />
                        </Grid>
                        <Grid item xs={5}>
                          <TextField
                            InputProps={{ readOnly: true }}
                            size="small"
                            label="Product Name"
                            fullWidth
                            value={name}
                          />
                        </Grid>

                        <Grid item xs={3}>
                          <TextField
                            size="small"
                            placeholder="Volume"
                            variant="outlined"
                            value={volume}
                            onChange={(e) => {
                              const next = new Map(productsInPrescription)
                              next.set(id, {
                                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                                volume: e.target.value as any,
                                code,
                                id,
                                name,
                                description,
                              })
                              setProductsInPrescription(next)
                            }}
                          />
                        </Grid>
                        <Grid item xs={1}>
                          <Button
                            onClick={() => {
                              const nextValues = new Map(productsInPrescription)
                              nextValues.delete(id)
                              setProductsInPrescription(nextValues)
                            }}
                          >
                            <Delete />
                          </Button>
                        </Grid>
                      </Grid>
                    ))}
                    <Grid item container alignItems="center" spacing={1}>
                      <Grid item xs>
                        <TextField
                          variant="outlined"
                          size="small"
                          placeholder="Notes"
                          fullWidth
                          value={notes}
                          onChange={(e) => {
                            setNotes(e.target.value)
                          }}
                          multiline
                          rows={2}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Grid>

            <Grid>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                bgcolor="#eaeaea"
                p="5px 30px"
                mt="20px"
              >
                <Typography>
                  <b>Issued by: </b>
                  {user.name}
                </Typography>
                <Button
                  disabled={notes === ''}
                  color="primary"
                  endIcon={<Send />}
                  onClick={() => {
                    setModalState('send_prescription')
                  }}
                >
                  Forward
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Layout>
  )
}
