import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import { useRequest } from 'ahooks'
import { useSnackbar } from 'notistack'
import React, { ReactNode, useState } from 'react'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'

type Checkout = {
  id: number
  duration: string
  first_name: string
  last_name: string
  gender: string
  birth_date: string
  email: string
  phone: string
  tracking_number: string
  shipping: string
  courier: string
  other_details: string
  street: string
  city: string
  country: string
  zip: string
  patient_id: string
  shipping_first_name: string
  shipping_last_name: string
  shipping_street: string
  shipping_city: string
  shipping_country: string
  shipping_zip: string
  shipping_other_details: string
}

export function DescriptionListItem({ label, value }: { label: string; value: ReactNode }) {
  return (
    <Grid container item spacing={2} alignItems="center">
      <Grid item xs={6}>
        <Typography align="right">{label}:</Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography align="left">{value}</Typography>
      </Grid>
    </Grid>
  )
}

function DescriptionList({ children }: { children: ReactNode }) {
  return (
    <Grid container spacing={1}>
      {children}
    </Grid>
  )
}

function Description({
  title,
  list,
}: {
  title: string
  list: Array<{ label: string; value: ReactNode }>
}) {
  return (
    <Grid item container spacing={1} direction="column">
      <Grid item>
        <Typography variant="body1" align="center">
          <b>{title}</b>
        </Typography>
      </Grid>

      <Grid item xs>
        <DescriptionList>
          {list.map((i) => (
            <DescriptionListItem key={i.label} {...i} />
          ))}
        </DescriptionList>
      </Grid>
    </Grid>
  )
}

interface CheckoutDetailsDialogProps {
  checkout: Checkout | undefined
  open: boolean
  onClose: () => void
}

export function useCheckoutDetails(patientId: string | number | undefined | null) {
  const api = useAPI()
  return useRequest<{ data: Checkout[] }>(() => api.requestResource(`/checkout/${patientId}`), {
    manual: !patientId,
  })
}

function CheckoutDetailsDialog({ checkout, open, onClose }: CheckoutDetailsDialogProps) {
  const api = useAPI()
  const { enqueueSnackbar } = useSnackbar()
  const sendDevicesRequest = useRequest<{ data: Checkout[] }>(
    () => api.requestJSON('POST', `/orders/send-devices/${checkout?.id}`),
    {
      manual: true,
      onSuccess: () => {
        enqueueSnackbar('Email has been sent to Customer and Doctor')
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onError: (e: any) => {
        if (e.error) {
          enqueueSnackbar(e.error, { variant: 'error' })
        } else {
          enqueueSnackbar(JSON.stringify(e), { variant: 'error' })
        }
      },
    }
  )

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md">
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs>
            Checkout Details for Delivery Order
          </Grid>
          <Grid>
            <IconButton
              onClick={() => {
                onClose()
              }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <Box paddingBottom={3}>
          <Grid container spacing={3}>
            <Grid item container spacing={3} xs={6}>
              <Description
                title="Subscription Details"
                list={[
                  {
                    label: 'Duration',
                    value: checkout?.duration ? `${checkout?.duration} Month programme` : '',
                  },
                  { label: 'Date/Time', value: '-' },
                ]}
              />

              <Description
                title="Personal Information"
                list={[
                  { label: 'First Name', value: checkout?.first_name },
                  { label: 'Last Name', value: checkout?.last_name },
                  { label: 'Email Address', value: checkout?.email },
                ]}
              />

              <Description
                title="Contact Information"
                list={[
                  { label: 'Email Address', value: checkout?.email },
                  { label: 'Contact NUmber', value: checkout?.phone },
                ]}
              />
            </Grid>

            <Grid item container spacing={3} xs={6}>
              <Description
                title="Billing Address"
                list={[
                  { label: 'Other Details', value: checkout?.other_details },
                  { label: 'Street', value: checkout?.street },
                  { label: 'City/State', value: checkout?.city },
                  { label: 'Country', value: checkout?.country },
                  { label: 'Zip Code', value: checkout?.zip },
                ]}
              />

              {!!checkout &&
                checkout.shipping_first_name !== '1' &&
                checkout.shipping_last_name !== '1' &&
                checkout.shipping_street !== '1' && (
                  <Description
                    title="Shipping Information"
                    list={[
                      {
                        label: 'First Name',
                        value: checkout.shipping_first_name || checkout.first_name,
                      },
                      {
                        label: 'Last Name',
                        value: checkout.shipping_last_name || checkout.last_name,
                      },
                      { label: 'Street', value: checkout.shipping_street || checkout.street },
                      { label: 'City/State', value: checkout.shipping_city || checkout.city },
                      { label: 'Country', value: checkout.shipping_country || checkout.country },
                      { label: 'Zip Code', value: checkout.shipping_zip || checkout.zip },
                    ]}
                  />
                )}

              <Box textAlign="center" width="100%">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => sendDevicesRequest.run()}
                  disabled={sendDevicesRequest.loading}
                >
                  Mark As Sent
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default function Orders() {
  const [checkoutToSend, setCheckoutToSend] = useState<Checkout>()

  return (
    <Layout title="Orders">
      <CheckoutDetailsDialog
        open={!!checkoutToSend}
        checkout={checkoutToSend}
        onClose={() => {
          setCheckoutToSend(undefined)
        }}
      />
      <DataTable<Checkout>
        key={checkoutToSend?.id}
        dataIndex="id"
        resource_url="/orders/"
        columns={[
          { label: 'Last Name', key: 'last_name' },
          { label: 'First Name', key: 'first_name' },
          { label: 'Email Address', key: 'email' },
          { label: 'Duration (Month)', key: 'duration' },
          {
            label: '',
            content: (c) => (
              <Button
                onClick={() => {
                  setCheckoutToSend(c)
                }}
              >
                Send
              </Button>
            ),
          },
        ]}
        toolbar={{
          search: false,
          pagination: true,
        }}
        disableCheckbox
      />
    </Layout>
  )
}
