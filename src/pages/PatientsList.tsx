import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import { useRequest } from 'ahooks'
import React, { ReactNode } from 'react'
import { Link, Route, useHistory, useParams } from 'react-router-dom'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'

type Checkout = {
  id: number
  duration: string
  first_name: string
  last_name: string
  gender: string
  birth_date: string
  email: string
  phone: string
  tracking_number: string
  shipping: string
  courier: string
  other_details: string
  street: string
  city: string
  country: string
  zip: string
  patient_id: string
  shipping_first_name: string
  shipping_last_name: string
  shipping_street: string
  shipping_city: string
  shipping_country: string
  shipping_zip: string
  shipping_other_details: string
}

export function DescriptionListItem({ label, value }: { label: string; value: ReactNode }) {
  return (
    <Grid container item spacing={2} alignItems="center">
      <Grid item xs={6}>
        <Typography align="right">{label}:</Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography align="left">{value}</Typography>
      </Grid>
    </Grid>
  )
}

function DescriptionList({ children }: { children: ReactNode }) {
  return (
    <Grid container spacing={1}>
      {children}
    </Grid>
  )
}

function Description({
  title,
  list,
}: {
  title: string
  list: Array<{ label: string; value: ReactNode }>
}) {
  return (
    <Grid item container spacing={1} direction="column">
      <Grid item>
        <Typography variant="body1" align="center">
          <b>{title}</b>
        </Typography>
      </Grid>

      <Grid item xs>
        <DescriptionList>
          {list.map((i) => (
            <DescriptionListItem key={i.label} {...i} />
          ))}
        </DescriptionList>
      </Grid>
    </Grid>
  )
}

interface Props {
  open: boolean
  onClose: () => void
}

export function useCheckoutDetails(patientId: string | number | undefined | null) {
  const api = useAPI()
  return useRequest<{ data: Checkout[] }>(() => api.requestResource(`/checkout/${patientId}`), {
    manual: !patientId,
  })
}

function PatientProfileDialog({ open, onClose }: Props) {
  const params = useParams<{ patientId: string }>()
  const patientChechoutDetails = useCheckoutDetails(params.patientId)
  const checkout = patientChechoutDetails.data?.data[0]

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md">
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs>
            Patient Details for Delivery Order
          </Grid>
          <Grid>
            <IconButton
              onClick={() => {
                onClose()
              }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <Box paddingBottom={3}>
          <Grid container spacing={3}>
            <Grid item container spacing={3} xs={6}>
              <Description
                title="Subscription Details"
                list={[
                  {
                    label: 'Duration',
                    value: checkout?.duration ? `${checkout?.duration} Month programme` : '',
                  },
                  { label: 'Date/Time', value: '-' },
                ]}
              />

              <Description
                title="Personal Information"
                list={[
                  { label: 'First Name', value: checkout?.first_name },
                  { label: 'Last Name', value: checkout?.last_name },
                  { label: 'Email Address', value: checkout?.email },
                ]}
              />

              <Description
                title="Contact Information"
                list={[
                  { label: 'Email Address', value: checkout?.email },
                  { label: 'Contact NUmber', value: checkout?.phone },
                ]}
              />
            </Grid>

            <Grid item container spacing={3} xs={6}>
              <Description
                title="Billing Address"
                list={[
                  { label: 'Other Details', value: checkout?.other_details },
                  { label: 'Street', value: checkout?.street },
                  { label: 'City/State', value: checkout?.city },
                  { label: 'Country', value: checkout?.country },
                  { label: 'Zip Code', value: checkout?.zip },
                ]}
              />

              {!!checkout &&
                checkout.shipping_first_name !== '1' &&
                checkout.shipping_last_name !== '1' &&
                checkout.shipping_street !== '1' && (
                  <Description
                    title="Shipping Information"
                    list={[
                      {
                        label: 'First Name',
                        value: checkout.shipping_first_name || checkout.first_name,
                      },
                      {
                        label: 'Last Name',
                        value: checkout.shipping_last_name || checkout.last_name,
                      },
                      { label: 'Street', value: checkout.shipping_street || checkout.street },
                      { label: 'City/State', value: checkout.shipping_city || checkout.city },
                      { label: 'Country', value: checkout.shipping_country || checkout.country },
                      { label: 'Zip Code', value: checkout.shipping_zip || checkout.zip },
                    ]}
                  />
                )}
            </Grid>
          </Grid>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type FIXME = any

export default function PatientsList() {
  const history = useHistory()

  return (
    <Layout title="Patient List">
      <Route path="/patients/profile/:patientId">
        {({ match }) => (
          <PatientProfileDialog
            open={!!match}
            key={match?.params.patientId}
            onClose={() => {
              history.goBack()
            }}
          />
        )}
      </Route>

      <DataTable<FIXME>
        dataIndex="id"
        resource_url="/customer-record/"
        columns={[
          { label: 'Client ID', key: 'id' },
          { label: 'Name', key: 'name' },
          { label: 'Email Address', key: 'email' },
          {
            label: '',
            content: ({ id }) => (
              <>
                <Link to={`/patients/profile/${id}`}>Profile</Link>
                <Box display="inline-block" marginX={1}>
                  |
                </Box>
                <Link to={`/patients/analysis/${id}`}>Analysis</Link>
              </>
            ),
          },
        ]}
        toolbar={{
          search: true,
          pagination: true,
        }}
        disableCheckbox
      />
    </Layout>
  )
}
