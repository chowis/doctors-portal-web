import { Box, Breadcrumbs, Button, Divider, Grid, Paper, Typography } from '@material-ui/core'
import { useRequest } from 'ahooks'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, Route, RouteComponentProps, useHistory, useParams } from 'react-router-dom'
import Viewer from 'react-viewer'
import { InferType } from 'yup'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { parseDateString } from '../helpers/dateHelpers'
import { numberSchema, objectSchema, stringSchema } from '../helpers/SchemaHelpers'
import { parseGender } from '../helpers/utils'

type AnalysisInformation = InferType<typeof analysisInformation>
const analysisInformation = objectSchema({
  batch_id: stringSchema(),
  customer_id: stringSchema(),
  date: stringSchema(),
  devicetype_id: stringSchema(),
  humidity: stringSchema(),
  id: stringSchema(),
  image_url: stringSchema(),
  measurement: stringSchema(),
  phone_model: stringSchema(),
  phone_os: stringSchema(),
  product_code: stringSchema(),
  score: stringSchema(),
  service_name: stringSchema(),
  temperature: stringSchema(),
  time: stringSchema(),
  uv_index: stringSchema(),
  version: stringSchema(),
  raw_value: stringSchema(),
})

type MoistureInformation = {
  measurement: string
  t_raw_value: string
  t_score: string
  u_raw_value: string
  u_score: string
}

type AnalysisHistoryDetails = InferType<typeof analysisHistoryDetailsSchema>
const analysisHistoryDetailsSchema = objectSchema({
  customer_details: objectSchema({
    birth: stringSchema(),
    country: stringSchema(),
    customer_humidity: stringSchema(),
    customer_temperature: stringSchema(),
    customer_uv_index: stringSchema(),
    email: stringSchema(),
    ethnicity: stringSchema(),
    gender: numberSchema(),
    id: numberSchema(),
    name: stringSchema(),
    phone: stringSchema(),
    skin_color: stringSchema(),
    surname: stringSchema(),
  }),
  device_details: objectSchema({
    device_id: stringSchema(),
    device_model: stringSchema(),
    os_system_code: stringSchema(),
    product_code: stringSchema(),
    service_name: stringSchema(),
    date: stringSchema(),
    time: stringSchema(),
  }),
})

function split(input: string, len: number) {
  return input.match(new RegExp(`.{1,${len}}(?=(.{${len}})+(?!.))|.{1,${len}}$`, 'g'))
}

export default function AnalysisDetailsPage() {
  const params = useParams<{ customer_id: string; analysis_id: string; batch_id: string }>()

  const history = useHistory()
  const api = useAPI()
  const { t } = useTranslation()
  const analysisHistoryDetails = useRequest<AnalysisHistoryDetails>(() =>
    api.requestResource(`/customer-record/${params.customer_id}/analysis-history/details/`, {
      params: {
        analysis_type: params.analysis_id,
        batch_id: params.batch_id,
      },
    })
  )

  return (
    <Layout
      title={
        <Breadcrumbs aria-label="breadcrumb">
          <Link to="/patients/">Patients</Link>
          <Link to={`/patients/analysis/${params.customer_id}`}>
            {t('sidebar.analysis_history')}
          </Link>
          <Typography display="initial">{t('sidebar.analysis_details')}</Typography>
        </Breadcrumbs>
      }
    >
      <Route path="/patients/:customer_id/:analysis_id/:batch_id/:image_url">
        {({ match }: RouteComponentProps<{ image_url: string } | null>) => (
          <Viewer
            visible={!!match}
            onClose={() => {
              history.push(
                `/patients/${params.customer_id}/${params.analysis_id}/${params.batch_id}/`
              )
            }}
            onMaskClick={() => {
              history.push(
                `/patients/${params.customer_id}/${params.analysis_id}/${params.batch_id}/`
              )
            }}
            images={[
              {
                src: `//images.weserv.nl/?url=${decodeURIComponent(
                  match?.params?.image_url || ''
                )}`,
                alt: 'Analysis Image',
              },
            ]}
          />
        )}
      </Route>

      <Box display="flex" justifyContent="flex-end" width="100%">
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            history.push(`/add-prescription/${params.customer_id}/${params.batch_id}/`)
          }}
        >
          Add Prescription
        </Button>
      </Box>

      <Box paddingY={2}>
        <Grid container spacing={2}>
          <Grid item sm>
            <Typography>
              <b>{t('analysis_details.customer_id')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.id}
            </Typography>
            <Typography>
              <b>{t('analysis_details.customer_name')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.surname}{' '}
              {analysisHistoryDetails.data?.customer_details.name}
            </Typography>
            <Typography>
              <b>{t('analysis_details.customer_birthdate')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.birth}
            </Typography>
          </Grid>
          <Grid item sm>
            <Typography>
              <b>{t('analysis_details.country')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.country}
            </Typography>
            <Typography>
              <b>{t('analysis_details.email')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.email}
            </Typography>
            <Typography>
              <b>{t('analysis_details.phone_number')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.phone}
            </Typography>
          </Grid>
          <Grid item sm>
            <Typography>
              <b>{t('analysis_details.skin_group')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.skin_color}
            </Typography>
            <Typography>
              <b>{t('analysis_details.ethnicity')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.ethnicity}
            </Typography>
            <Typography>
              <b>{t('analysis_details.gender')}:</b>{' '}
              {analysisHistoryDetails.data?.customer_details.gender
                ? parseGender(analysisHistoryDetails.data.customer_details.gender)
                : null}
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Typography>
        <b>{t('analysis_details.measurement_details.title')}</b>
      </Typography>
      <Paper variant="outlined">
        <Box paddingX={2} paddingY={1}>
          <Grid container>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.device_id')}:</b>{' '}
                {analysisHistoryDetails.data?.device_details.device_id}
              </Typography>
            </Grid>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.product_code')}:</b>{' '}
                {analysisHistoryDetails.data?.device_details.product_code}
              </Typography>
            </Grid>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.phone_model')}:</b>{' '}
                {analysisHistoryDetails.data?.device_details.device_model}
              </Typography>
            </Grid>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.phone_os')}:</b>{' '}
                {analysisHistoryDetails.data?.device_details.os_system_code}
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Divider />
        <Box paddingX={2} paddingY={1}>
          <Grid container>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.batch_id')}:</b> {params.batch_id}
              </Typography>
            </Grid>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.app_name')}:</b>{' '}
                {analysisHistoryDetails.data?.device_details.service_name}
              </Typography>
            </Grid>
            <Grid item>
              <Typography>
                <b>{t('analysis_details.measurement_details.date_time')}:</b>{' '}
                {parseDateString(analysisHistoryDetails.data?.device_details.date || '')} /{' '}
                {split(analysisHistoryDetails.data?.device_details.time || '', 2)?.join(':')}
              </Typography>
            </Grid>
          </Grid>
        </Box>

        <Divider />
        <Box paddingX={2} paddingY={1}>
          <Grid container>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.temperature')}:</b>{' '}
                {analysisHistoryDetails.data?.customer_details.customer_temperature}
              </Typography>
            </Grid>
            <Grid item sm={3}>
              <Typography>
                <b>{t('analysis_details.measurement_details.humidity')}:</b>{' '}
                {analysisHistoryDetails.data?.customer_details.customer_humidity}
              </Typography>
            </Grid>
            <Grid item>
              <Typography>
                <b>{t('analysis_details.measurement_details.uv_index')}:</b>{' '}
                {analysisHistoryDetails.data?.customer_details.customer_uv_index}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Paper>

      <Box marginBottom={1} marginTop={4}>
        <Typography>
          <b>Moisture Information</b>
        </Typography>
      </Box>

      <DataTable<MoistureInformation>
        // dataIndex="id"
        disableCheckbox
        resource_url="/customer-record/hydration-sebum/"
        params={{
          customer_id: params.customer_id,
          analysis_type: params.analysis_id,
          batch_id: params.batch_id,
        }}
        columns={[
          { label: t('analysis_details.analysis_information.measurement'), key: 'measurement' },
          { label: 'T Zone Score', key: 't_score' },
          { label: 'U Zone Score', key: 'u_score' },
          { label: 'T Zone Raw Value', key: 't_raw_value' },
          { label: 'U Zone Raw Value', key: 'u_raw_value' },
          {
            label: t('analysis_details.analysis_information.image'),
            content: () => '-',
          },
        ]}
        toolbar={{
          pagination: false,
        }}
      />

      <Box marginBottom={1} marginTop={4}>
        <Typography>
          <b>{t('analysis_details.analysis_information.title')}</b>
        </Typography>
      </Box>

      <DataTable<AnalysisInformation>
        dataIndex="id"
        disableCheckbox
        resource_url={`/customer-record/${params.customer_id}/analysis-history/analysis-infor`}
        normalize={
          (data) =>
            data?.map((i, idx) => {
              const prevItemsLength = data.slice(0, idx).reduce((sum, curr) => {
                if (curr.measurement === i.measurement) {
                  return sum + 1
                }
                return sum
              }, 0)

              return { ...i, measurement: `${i.measurement} ${prevItemsLength + 1}` }
            })
          // eslint-disable-next-line react/jsx-curly-newline
        }
        params={{
          analysis_type: params.analysis_id,
          batch_id: params.batch_id,
        }}
        columns={[
          { label: t('analysis_details.analysis_information.measurement'), key: 'measurement' },
          { label: t('analysis_details.analysis_information.score'), key: 'score' },
          {
            label: t('analysis_details.analysis_information.raw_value'),
            content: ({ raw_value }) => raw_value || '-',
            hide: true,
          },
          {
            label: t('analysis_details.analysis_information.image'),
            content: ({ image_url }) =>
              image_url ? (
                <Link
                  to={`/patients/${params.customer_id}/${params.analysis_id}/${
                    params.batch_id
                  }/${encodeURIComponent(image_url)}`}
                >
                  {t('analysis_details.analysis_information.view_details')}
                </Link>
              ) : (
                <Typography color="textSecondary">
                  {t('analysis_details.analysis_information.no_img')}
                </Typography>
              ),
          },
        ]}
        toolbar={{
          pagination: false,
        }}
      />
    </Layout>
  )
}
