/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable react/no-unused-prop-types */
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  Link as AnchorLink,
  MenuItem,
  TextField,
  Typography,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import { useRequest } from 'ahooks'
import { useSnackbar } from 'notistack'
import React, { useMemo, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { Link, Route, useParams } from 'react-router-dom'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { useAccessFlags } from '../data/AccessFlags'
import { objectSchema, stringSchema } from '../helpers/SchemaHelpers'
import { parseGender } from '../helpers/utils'
import { DescriptionListItem, useCheckoutDetails } from './PatientsList'

type PrescriptionDetail = {
  prescription_id: number
  patient_id: number
  batch_id: number
  products: [
    {
      id: number
      volume: number
    }
  ]
  notes: string
  date_time: string
  status: string
  pharmacist_email: string
  logisctics_email: string
  client_name: string
}

type Prescription = {
  address: string
  doctor_name: string
  email: string
  gender: number
  name: string
  phone: string
  recommendations: Recommendation[]
}

type SendCheckoutToPatient = {
  batch_id: number | string
  patient_id: number | string
  products_length: number
  email: string
  checkout_id: number
  prescription_id: number
  tracking_number: number | string
  courier: string
  shipping_first_name: string
  shipping_last_name: string
  shipping_street: string
  shipping_city: string
  shipping_country: string
  shipping_zip: string
  shipping_other_details: string
}

type Logistics = {
  Branch_id: number
  Company_id: number
  Rank_id: number
  Store_id: number
  address: null | string
  birthdate: null | string
  city: null | string
  country: null | string
  email: string
  gender: string
  id: number
  is_active: number
  loginserver_id: number
  memo: string
  name: string
  phone: string
  programs_id: number
  register_date: string
  surname: string
}

type Recommendation = { id: number; name: string; description: string }

interface PrescriptionDialogProps {
  open: boolean
  patient_id: number
  batch_id: number | string
  onClose: () => void
}

function PrescriptionDialog({ open, patient_id, batch_id, onClose }: PrescriptionDialogProps) {
  // :)
  const params = useParams<{ prescriptionId: string }>()
  const { requestResource } = useAPI()
  const response = useRequest(
    () =>
      requestResource<Prescription[]>(`/prescriptions/details/${params.prescriptionId}/`, {
        params: { batch_id },
      }),
    { manual: !params.prescriptionId }
  )
  const checkoutDetails = useCheckoutDetails(patient_id)
  const checkout = checkoutDetails.data?.data[0]

  const data = response.data?.data[0]
  const recommendations = useMemo(() => data?.recommendations, [data])

  return (
    <Dialog open={open} onClose={onClose} maxWidth="lg">
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs>
            Prescription Details
          </Grid>
          <Grid>
            <IconButton
              onClick={() => {
                onClose()
              }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <Box padding={3} minWidth="400px">
          <DescriptionListItem label="Email to" value="Pharmacist" />
          <DescriptionListItem label="Subject" value="Patient Name" />
          <Divider />
          <DescriptionListItem label="Name" value={data?.name} />
          <DescriptionListItem label="Gender" value={parseGender(data?.gender)} />
          <DescriptionListItem label="Email Address" value={data?.email} />
          <DescriptionListItem label="Contact Number" value={data?.phone} />
          <DescriptionListItem label="Address" value={data?.address} />
          {checkout?.shipping_street && (
            <DescriptionListItem
              label="Shipping Address"
              value={`${checkout?.shipping_street} ${checkout?.shipping_zip}, ${checkout?.shipping_city}, ${checkout?.country} `}
            />
          )}
          <Divider />

          <Box paddingBottom={2} />
          <Typography variant="body1">
            <b>Product Recommendation</b>
          </Typography>
          <Box bgcolor="#e6e6e6" borderRadius="4px" padding={1}>
            {recommendations?.map(({ id, name, description }) => (
              <Grid key={id} container spacing={2}>
                <Grid item md={4}>
                  {id}
                </Grid>
                <Grid item md={4}>
                  {name}
                </Grid>
                <Grid item md={4}>
                  {description}
                </Grid>
              </Grid>
            ))}
          </Box>

          <Box paddingBottom={2} />
          <Typography variant="body2">
            <b>Notes</b>
          </Typography>
          <Box padding={1}>{(data as any)?.notes || 'No Notes'}</Box>

          <Box paddingBottom={2} />
          <Typography variant="body1">
            <b>{data?.doctor_name}</b> <em>{data?.phone}</em>
          </Typography>
          <Divider />

          <Box textAlign="center" marginTop={3}>
            <Button variant="contained" onClick={onClose}>
              Dismiss
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

type SendLogisticsRequest = {
  prescription_id: number
  logistics_email: string
  logistics_name: string
  patient_id: number
  batch_id: number
  products_length: number
}

interface SendToLogisticsModalProps {
  open: boolean
  prescription: PrescriptionDetail | undefined
  onClose: () => void
  onSuccess?: () => void
}

function SendToLogisticsModal({
  prescription,
  open,
  onClose,
  onSuccess,
}: SendToLogisticsModalProps) {
  const form = useForm({
    resolver: yupResolver(objectSchema({ email: stringSchema().email().required() })),
  })
  const api = useAPI()

  const logistics = useRequest<{ data: Logistics[] }>(() =>
    api.requestResource('/customer-record/logistics')
  )

  const logisticsPeople = logistics.data?.data || []

  const sendCheckoutToLogisticsRequest = useRequest(
    (s: SendLogisticsRequest) =>
      api.requestJSON('POST', '/prescriptions/send-to-logistics', { data: s }),
    {
      manual: true,
      onSuccess,
    }
  )

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs>
            Send To Logistics
          </Grid>
          <Grid>
            <IconButton
              onClick={() => {
                onClose()
              }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>

      <form
        onSubmit={form.handleSubmit((values) => {
          const logisticsPerson = logisticsPeople.find((l) => l.email === values.email)
          if (prescription?.prescription_id) {
            sendCheckoutToLogisticsRequest.run({
              logistics_email: values.email,
              prescription_id: prescription.prescription_id,
              batch_id: prescription.batch_id,
              patient_id: prescription.patient_id,
              logistics_name: logisticsPerson?.name || logisticsPerson?.surname || '',
              products_length: prescription.products.length,
            })
          }
        })}
      >
        <DialogContent>
          <Box width="300px">
            <Controller
              name="email"
              control={form.control}
              render={({ onChange, value, ref }) => (
                <TextField
                  select
                  inputRef={ref}
                  fullWidth
                  label="Select Logistics"
                  defaultValue=""
                  value={value}
                  onChange={(event) => onChange(event.target.value)}
                >
                  {logistics.data?.data.map((p) => (
                    <MenuItem key={p.id} value={p.email}>
                      {p.email}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Box margin={2}>
            <Grid container spacing={2} alignItems="center">
              {sendCheckoutToLogisticsRequest.loading && (
                <Grid item>
                  <CircularProgress size={24} />
                </Grid>
              )}
              <Grid item>
                <Button
                  disabled={sendCheckoutToLogisticsRequest.loading}
                  variant="outlined"
                  fullWidth
                  type="submit"
                >
                  Send
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  )
}

function SendToPatientModal({ open, prescription, onClose, onSuccess }: SendToLogisticsModalProps) {
  const form = useForm({
    defaultValues: {
      tracking_number: '',
      courier: '',
    },
    resolver: yupResolver(
      objectSchema({
        tracking_number: stringSchema().required(),
        courier: stringSchema().required(),
      })
    ),
  })
  const api = useAPI()
  const { enqueueSnackbar } = useSnackbar()
  const checkoutDetails = useCheckoutDetails(prescription?.patient_id)
  const checkout = checkoutDetails.data?.data[0]

  const sendCheckoutToPatientRequest = useRequest(
    (s: SendCheckoutToPatient) =>
      api.requestJSON('POST', '/prescriptions/send-checkout/', { data: s }),
    {
      manual: true,
      onSuccess: () => {
        onSuccess?.()
      },
      onError: (e) => {
        enqueueSnackbar(JSON.stringify(e), {
          variant: 'error',
        })
      },
    }
  )

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs>
            Send To Patient
          </Grid>
          <Grid>
            <IconButton
              onClick={() => {
                onClose()
              }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>

      <form
        onSubmit={form.handleSubmit((values) => {
          const patientEmail = checkout?.email || ''

          if (!patientEmail) {
            enqueueSnackbar('There is no patient email')
          }

          if (!checkout) {
            enqueueSnackbar('There is no checkout information')
          }

          if (patientEmail && checkout && prescription) {
            const {
              shipping_city,
              shipping_country,
              shipping_first_name,
              shipping_last_name,
              shipping_other_details,
              shipping_street,
              shipping_zip,
            } = checkout

            sendCheckoutToPatientRequest.run({
              email: patientEmail,
              products_length: prescription.products.length,
              batch_id: prescription.batch_id,
              patient_id: prescription.patient_id,
              checkout_id: checkout.id,
              prescription_id: prescription.prescription_id,
              courier: values.courier,
              tracking_number: values.tracking_number,
              shipping_city,
              shipping_country,
              shipping_first_name,
              shipping_last_name,
              shipping_other_details,
              shipping_street,
              shipping_zip,
            })
          }
        })}
      >
        <DialogContent>
          <Box width="300px">
            <TextField
              fullWidth
              name="tracking_number"
              inputRef={form.register()}
              label="Tracking Number"
              error={!!form.errors?.tracking_number}
              helperText={form.errors?.tracking_number?.message}
            />

            <Box mt={2} />

            <TextField
              fullWidth
              name="courier"
              inputRef={form.register()}
              label="Courier"
              error={!!form.errors?.courier}
              helperText={form.errors?.courier?.message}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Box margin={2}>
            <Grid container spacing={2} alignItems="center">
              {sendCheckoutToPatientRequest.loading && (
                <Grid item>
                  <CircularProgress size={24} />
                </Grid>
              )}
              <Grid item>
                <Button
                  // disabled={sendCheckoutToPatientRequest.loading}
                  variant="outlined"
                  fullWidth
                  type="submit"
                >
                  Send
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export function PrescriptionDetails() {
  const [modalState, setModalState] = useState<'sent_to_logistics' | 'sent_to_patient'>()
  const [prescriptionToSend, setPrescriptionToSend] = useState<PrescriptionDetail>()
  const { enqueueSnackbar } = useSnackbar()
  const { can_send_to_patient } = useAccessFlags()

  const closeModal = () => {
    setModalState(undefined)
  }

  return (
    <Layout title="Prescription Details">
      <Route path="/prescription-details/view/:prescriptionId/:patientId/:batch_id">
        {({ match, history }) => (
          <PrescriptionDialog
            open={!!match}
            patient_id={match?.params.patientId}
            batch_id={match?.params.batch_id}
            key={match?.params.prescriptionId}
            onClose={() => {
              history.goBack()
            }}
          />
        )}
      </Route>

      <SendToLogisticsModal
        key={prescriptionToSend?.prescription_id}
        prescription={prescriptionToSend}
        open={modalState === 'sent_to_logistics'}
        onClose={closeModal}
        onSuccess={() => {
          enqueueSnackbar('Successfully sent', {
            variant: 'success',
          })
          setTimeout(() => {
            closeModal()
          }, 1000)
        }}
      />

      <SendToPatientModal
        key={prescriptionToSend?.prescription_id}
        prescription={prescriptionToSend}
        open={modalState === 'sent_to_patient'}
        onSuccess={() => {
          enqueueSnackbar('Successfully sent', {
            variant: 'success',
          })
          setTimeout(() => {
            closeModal()
          }, 1000)
        }}
        onClose={closeModal}
      />

      <DataTable<PrescriptionDetail>
        dataIndex="prescription_id"
        resource_url="/prescriptions"
        columns={[
          { label: 'Prescription ID', key: 'prescription_id' },
          { label: 'Batch ID', key: 'batch_id' },
          { label: 'Client Name', key: 'client_name' },
          {
            label: '',
            content: (prescription) => (
              <>
                <Link
                  to={`/prescription-details/view/${prescription.prescription_id}/${prescription.patient_id}/${prescription.batch_id}`}
                >
                  View Details
                </Link>
                <Box display="inline-block" marginX={1}>
                  |
                </Box>
                <AnchorLink
                  href="#"
                  onClick={() => {
                    setModalState(can_send_to_patient ? 'sent_to_patient' : 'sent_to_logistics')
                    setPrescriptionToSend(prescription)
                  }}
                >
                  Send
                </AnchorLink>
              </>
            ),
          },
        ]}
        toolbar={{
          search: true,
          pagination: true,
        }}
        disableCheckbox
      />
    </Layout>
  )
}
