import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Table,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@material-ui/core'
import { useRequest } from 'ahooks'
import * as countries from 'i18n-iso-countries'
import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { TableWrapper } from '../components/TableWrapper'
import { useAppLanguage } from '../i18n/hooks'

countries.registerLocale(require('i18n-iso-countries/langs/en.json'))

export type Product = {
  code: string
  description: string
  id: string
  name: string
  volume: number | null
}

export default function AddProducts() {
  const api = useAPI()
  const { t } = useTranslation()
  const [productId, setProductId] = useState<string>()
  const { enqueueSnackbar } = useSnackbar()
  const [currentLanguage] = useAppLanguage()
  const [key, setKey] = useState<number>()

  const { register, reset, handleSubmit, formState, setValue } = useForm<Product>({
    defaultValues: { id: '', name: '', description: '', code: '' },
  })

  const refetchProducts = () => {
    // workaround
    setKey(Math.random())
  }

  const parseError = (error: unknown) => {
    if (typeof error === 'object' && error) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const message = (error as any).message[currentLanguage]
      return message as string
    }
    return undefined
  }

  const addProductRequest = useRequest(
    (product) => api.requestJSON('POST', '/products', { data: product }),
    {
      manual: true,
      onSuccess: () => {
        reset()
        refetchProducts()
      },
      onError: (error) => {
        const errorMessage = parseError(error)
        enqueueSnackbar(
          <Box maxWidth="500px">
            <Typography variant="h5">Failed to create product</Typography>
            <Typography variant="body2">{errorMessage}</Typography>
          </Box>,
          { variant: 'error' }
        )
      },
    }
  )

  const editProductRequest = useRequest(
    (product) => api.requestJSON('PUT', '/products', { data: product }),
    {
      manual: true,
      onSuccess: () => {
        reset()
        refetchProducts()
      },
      onError: (error) => {
        const errorMessage = parseError(error)
        enqueueSnackbar(
          <Box maxWidth="500px">
            <Typography variant="h5">Failed to update product</Typography>
            <Typography variant="body2">{errorMessage}</Typography>
          </Box>,
          { variant: 'error' }
        )
      },
    }
  )

  const removeProductRequest = useRequest(
    (product_id) => api.requestJSON('DELETE', '/products', { data: { product_id } }),
    {
      manual: true,
      onSuccess: () => {
        reset()
        refetchProducts()
      },
      onError: (error) => {
        const errorMessage = parseError(error)
        enqueueSnackbar(
          <Box maxWidth="500px">
            <Typography variant="h5">Failed to remove product</Typography>
            <Typography variant="body2">{errorMessage}</Typography>
          </Box>,
          { variant: 'error' }
        )
      },
    }
  )

  const onSubmit = ({ id, ...values }: Product) => {
    if (id) {
      return editProductRequest.run({ product_id: id, volume: 1, ...values })
    }

    return addProductRequest.run({ volume: 1, ...values })
  }

  useEffect(() => {
    register({ name: 'id' })
  }, [register])

  return (
    <Layout title="Add Products">
      <Dialog
        open={!!productId}
        onClose={() => {
          setProductId('')
        }}
      >
        <DialogTitle>Are you sure you to remove this product?</DialogTitle>
        <DialogContent>
          <Box marginBottom={2}>
            <Grid container spacing={2}>
              <Grid item xs>
                <Button
                  fullWidth
                  variant="outlined"
                  onClick={() => {
                    setProductId('')
                  }}
                >
                  {t('login.no')}
                </Button>
              </Grid>
              <Grid item xs>
                <Button
                  fullWidth
                  color="secondary"
                  variant="outlined"
                  onClick={() => {
                    removeProductRequest.run(productId)
                    setProductId(undefined)
                  }}
                >
                  {t('login.yes')}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogContent>
      </Dialog>
      <Box>
        <form onSubmit={handleSubmit(onSubmit)}>
          <TableWrapper style={{ maxHeight: '100%', overflowX: 'initial', position: 'relative' }}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>
                    <TextField
                      name="name"
                      inputRef={register}
                      placeholder="Product Name"
                      variant="outlined"
                      size="small"
                      disabled={formState.isSubmitting}
                    />
                  </TableCell>
                  <TableCell>
                    <TextField
                      name="code"
                      inputRef={register}
                      placeholder="Product Code"
                      variant="outlined"
                      size="small"
                      disabled={formState.isSubmitting}
                    />
                  </TableCell>
                  <TableCell>
                    <TextField
                      name="description"
                      inputRef={register}
                      placeholder="Description"
                      variant="outlined"
                      size="small"
                      fullWidth
                      disabled={formState.isSubmitting}
                    />
                  </TableCell>
                  <TableCell>
                    <Button
                      disabled={formState.isSubmitting || !formState.isDirty}
                      type="submit"
                      variant="outlined"
                      endIcon={formState.isSubmitting ? <CircularProgress size={16} /> : undefined}
                    >
                      Save
                    </Button>
                    <Button disabled={formState.isSubmitting} onClick={() => reset()}>
                      Clear
                    </Button>
                  </TableCell>
                </TableRow>
              </TableHead>
            </Table>
          </TableWrapper>
        </form>
      </Box>

      <DataTable<Product>
        dataIndex="code"
        key={key}
        resource_url="/products/"
        columns={[
          { label: 'Product Name', key: 'name' },
          { label: 'Product Code', key: 'code' },
          { label: 'Description', key: 'description' },
          {
            label: '',
            content: ({ name, code, description, id }) => (
              <>
                <Button
                  onClick={() => {
                    setValue('name', name)
                    setValue('code', code)
                    setValue('description', description)
                    setValue('id', id)
                  }}
                >
                  Edit
                </Button>
                <Button
                  onClick={() => {
                    setProductId(id)
                  }}
                >
                  Remove
                </Button>
              </>
            ),
          },
        ]}
        toolbar={{
          pagination: true,
        }}
        disableCheckbox
      />
    </Layout>
  )
}
